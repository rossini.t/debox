#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ssh vagrant@localhost -p 4444 -i "$DIR/.vagrant/machines/default/virtualbox/private_key"
